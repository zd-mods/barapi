package ru.zoom4ikdan4ik.barapi.managers;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import org.lwjgl.opengl.GL11;

public class RenderManager {
	private final Minecraft minecraft = Minecraft.getMinecraft();
	public final ResourceLocation location = new ResourceLocation("Bar:bar.png");
	public boolean draw = false;
	public String message = "";
	public double percents;

	@SubscribeEvent(priority = EventPriority.NORMAL)
	public void eventHandler(RenderGameOverlayEvent.Post event) {
		if (event.type == RenderGameOverlayEvent.ElementType.ALL) {
			ScaledResolution f = new ScaledResolution(this.minecraft, this.minecraft.displayWidth,
					this.minecraft.displayHeight);

			if ((this.message != null) && (message.length() > 0))
				this.minecraft.ingameGUI.drawCenteredString(this.minecraft.fontRenderer, this.message,
						f.getScaledWidth() / 2, 4, 16777215);

			if (this.draw) {
				int width = f.getScaledWidth();
				this.minecraft.getTextureManager().bindTexture(this.location);
				GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
				drawModal(width / 4.4D - 182 / 2.0D, 10.0D, 182, 5.0D, 0.0D, 0.0D, 1.0D, 0.5D);
				if ((this.percents >= 0.0D) && (this.percents <= 100.0D)) {
					double wi = 182 / 100.0D * this.percents;
					drawModal(width / 4.4D - 182 / 2.0D, 10.0D, wi, 5.0D, 0.0D, 0.5D, this.percents / 100.0D, 1.0D);
				}
			}
		}
	}

	public void drawModal(double x, double y, double w, double h, double arr0, double arr1, double arr2, double arr3) {
		new ScaledResolution(this.minecraft, this.minecraft.displayWidth, this.minecraft.displayHeight);
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawingQuads();
		tessellator.addVertexWithUV(x, y + h, 0.0D, arr0, arr3);
		tessellator.addVertexWithUV(x + w, y + h, 0.0D, arr2, arr3);
		tessellator.addVertexWithUV(x + w, y, 0.0D, arr2, arr1);
		tessellator.addVertexWithUV(x, y, 0.0D, arr0, arr1);
		GL11.glPushMatrix();
		GL11.glScaled(2.2D, 2.0D, 2.2D);
		tessellator.draw();
		GL11.glPopMatrix();
	}
}
