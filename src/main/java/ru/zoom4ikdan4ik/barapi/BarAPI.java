package ru.zoom4ikdan4ik.barapi;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.network.FMLEventChannel;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraftforge.common.MinecraftForge;
import ru.zoom4ikdan4ik.barapi.handlers.PacketHandler;
import ru.zoom4ikdan4ik.barapi.managers.ConfigManager;
import ru.zoom4ikdan4ik.barapi.managers.RenderManager;

@Mod(modid = ConfigManager.MODID, name = ConfigManager.MODID)
public class BarAPI {
    private final FMLEventChannel bar = NetworkRegistry.INSTANCE.newEventDrivenChannel(ConfigManager.CHANNEL_BAR);
    private final FMLEventChannel api = NetworkRegistry.INSTANCE.newEventDrivenChannel(ConfigManager.CHANNEL_API);

    private final PacketHandler handler = new PacketHandler();

    public final RenderManager render = new RenderManager();

    @Instance(ConfigManager.MODID)
    public static BarAPI instance;

    @SideOnly(Side.CLIENT)
    @EventHandler
    public void initialization(FMLInitializationEvent event) {
        this.bar.register(this.handler);
        this.api.register(this.handler);

        MinecraftForge.EVENT_BUS.register(this.handler);
        MinecraftForge.EVENT_BUS.register(this.render);
    }
}
