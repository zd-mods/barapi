package ru.zoom4ikdan4ik.barapi.handlers;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.network.FMLNetworkEvent.ClientCustomPacketEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.netty.buffer.ByteBuf;
import ru.zoom4ikdan4ik.barapi.BarAPI;
import ru.zoom4ikdan4ik.barapi.managers.ConfigManager;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;

@SideOnly(Side.CLIENT)
public class PacketHandler {

	@SubscribeEvent
	public void onClientPacket(ClientCustomPacketEvent event) throws UnsupportedEncodingException {
		String channel = event.packet.channel();
		ByteBuf buf = event.packet.payload();
		String data = new String(buf.array(), StandardCharsets.UTF_8);

		switch (channel) {
		case ConfigManager.CHANNEL_API:
			if (data == null || (data != null && data.isEmpty()))
				BarAPI.instance.render.message = "";
			else
				BarAPI.instance.render.message = this.h2s(data, true);

			break;
		case ConfigManager.CHANNEL_BAR:
			if (data == null || (data != null && data.isEmpty())) {
				BarAPI.instance.render.draw = false;
				BarAPI.instance.render.percents = 0;
			} else {
				try {
					BarAPI.instance.render.draw = true;
					BarAPI.instance.render.percents = Double.parseDouble(this.h2s(data, false));
				} catch (Exception var5) {
					BarAPI.instance.render.draw = false;
					BarAPI.instance.render.percents = 0;
				}
			}

			break;
		}
	}

	private String h2s(String hex, boolean r) {
		if (hex == null) {
			return "";
		}
		if (!hex.matches("[0-9a-f]+")) {
			return hex;
		}
		byte[] s1 = new BigInteger(hex, 16).toByteArray();
		byte[] s2 = new byte[s1.length];
		for (int var7 = 0; var7 < s1.length; var7++) {
			s2[var7] = s1[var7];
		}
		String var71 = null;
		var71 = new String(s2, StandardCharsets.UTF_8);
		if (r) {
			var71 = var71.substring(1);
		}
		return var71;
	}
}
